// app.js

var express = require("express");
var Unblocker = require("unblocker");
const morgan = require("morgan");
const { createProxyMiddleware } = require('http-proxy-middleware');

//Vars
const API_SERVICE_URL = "https://jsonplaceholder.typicode.com";

// Create Express Server
var app = express();

// Our User-Agent Middleware
function setUserAgent(data) {
  data.headers["user-agent"] =
    "Mozilla/5.0 (iPad; CPU OS 12_2 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148";
}

function attachAuth(data) {
  if (data.url.match(/^https?:\/\/instagram.com\//)) {
      data.headers["x-instagram-token"] = "123";
  }
}
function dropCookies(data) {
  if (data.url.match(/^https?:\/\/instagram.com\//)) {
      var cookies = setCookie.parse(data, { decodeValues: false });
      if (cookies.length) {
        debug("filtering set-cookie headers");
        data.headers["set-cookie"] = cookies.filter(function (cookie) {
          if (cookie.name.includes("bad_cookie")){
              return false;
          }
          return true;
        });
      }
  }
}

// Create Unblocker Instance
var unblocker = new Unblocker({
  prefix: "/proxy/",
  requestMiddleware: [
    setUserAgent, // Enable User Agent Middleware
    attachAuth, //add auth Info
    dropCookies //drop unwanted cookies from ever reaching our web scrapers
  ],
});

// Array to store the connected websites
const connectedWebsites = ["test"];

// Middleware to track the connected websites
app.use((req, res, next) => {
  //const url = req.query.url;
  const url = req.url;

  console.log(url);
  if (url) {
    console.log(url);
    connectedWebsites.push(url);
  }
  next();
});

// Configure Our Express Server to Use It
app.use(unblocker);

// Logging
app.use(morgan('dev'));



//may use auth
// Authorization
/* app.use('', (req, res, next) => {
  if (req.headers.authorization) {
      next();
  } else {
      res.sendStatus(403);
  }
}); */

//#################################################

app.get("/", (req, res) =>
  res.redirect("/proxy/https://en.wikipedia.org/wiki/Main_Page")
);

// Info GET endpoint
app.get('/info', (req, res, next) => {
  res.send('This is a test');
});

// Route to show the connected websites
app.get("/connected-websites", (req, res) => {
  res.json(connectedWebsites);
});


// Proxy endpoints https://www.twilio.com/blog/node-js-proxy-server
app.use('/json_placeholder', createProxyMiddleware({
  target: API_SERVICE_URL,
  changeOrigin: true,
  pathRewrite: {
      [`^/json_placeholder`]: '',
  },
}));


// Launches Server on Port 8080
app.listen(process.env.PORT || 8080).on("upgrade", unblocker.onUpgrade);
console.log("Node Unblocker Server Running On Port:", process.env.PORT || 8080);

//run http://localhost:8080/proxy/https://www.amazon.com/
    //http://localhost:8080/json_placeholder/posts/3