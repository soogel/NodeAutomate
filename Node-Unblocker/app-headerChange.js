// app.js

var express = require('express')
var Unblocker = require('unblocker');

// Create Express Server
var app = express();


// Our User-Agent Middleware
function setUserAgent(data) {
    data.headers["user-agent"] = "Mozilla/5.0 (iPad; CPU OS 12_2 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148";

    if (data.url.match(/^https?:\/\/google.com\//)) {
      data.headers["user-agent"] = "Mozilla/5.0 (iPad; CPU OS 12_2 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148";
    } 
}

// Create Unblocker Instance
var unblocker = new Unblocker(
  {
    prefix: '/proxy/',
    requestMiddleware: [
      setUserAgent  // Enable User Agent Middleware
    ] 
  });

// Configure Our Express Server to Use It
app.use(unblocker);


// Launches Server on Port 8080
app.listen(process.env.PORT || 8080).on('upgrade', unblocker.onUpgrade);
console.log("Node Unblocker Server Running On Port:", process.env.PORT || 8080)


//run http://localhost:8080/proxy/https://www.amazon.com/