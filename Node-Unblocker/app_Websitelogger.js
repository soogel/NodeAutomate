
const unblocker = require('unblocker');

// Create an instance of the unblocker
const app = unblocker({
  prefix: '/unblocker', // The URL prefix for the unblocker routes
  userAgent: 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36', // The user agent to use when making requests
});

// Array to store the connected websites
const connectedWebsites = [];

// Middleware to track the connected websites
app.use((req, res, next) => {
  const url = req.query.url;
  if (url) {
    connectedWebsites.push(url);
  }
  next();
});

// Route to show the connected websites
app.get('/connected-websites', (req, res) => {
  res.json(connectedWebsites);
});

// Start the server
app.listen(3000, () => {
  console.log('Unblocker server is running on port 3000');
});


