const cron = require('node-cron');
const executeTasks = require('./tasks/executeTasks');

cron.schedule('* * * * *', () => {
    executeTasks();
});
