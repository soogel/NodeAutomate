const { Client, LocalAuth } = require('whatsapp-web.js');
const qrcode = require('qrcode-terminal');
const connectDatabase = require('./database');
const executeFollowUpTask = require('./tasks/followUpTask');
const executeMessageTask = require('./tasks/messageTask');
const executeGroupTask = require('./tasks/groupTask');
const executeSyncTask = require('./tasks/syncTask');
const executeReminderTask = require('./tasks/reminderTask');
const executeSupportRequestTask = require('./tasks/supportRequestTask');
const executeDataSyncTask = require('./tasks/dataSyncTask');
const executeReportTask = require('./tasks/reportTask');
const executeSurveyTask = require('./tasks/surveyTask');

const client = new Client({
    authStrategy: new LocalAuth({ clientId: "client-one", dataPath: "./.wwebjs_auth" }),
    puppeteer: { headless: true }
});

client.on('qr', (qr) => {
    qrcode.generate(qr, { small: true });
});

client.on('ready', async () => {
    console.log('Client ist bereit!');
    
    // Datenbankverbindung herstellen
    await connectDatabase();
    
    // Beispielaufgaben erstellen
    const Task = require('./models/task');
    
    const fiveHoursFromNow = new Date(Date.now() + 5 * 60 * 60 * 1000);
    await new Task({
        type: 'message',
        data: { to: '1234567890@c.us', text: 'Nachricht in 5 Stunden' },
        scheduledAt: fiveHoursFromNow
    }).save();
    
    const oneHourFromNow = new Date(Date.now() + 1 * 60 * 60 * 1000);
    await new Task({
        type: 'reminder',
        data: { to: '1234567890@c.us', text: 'Erinnerung in 1 Stunde' },
        scheduledAt: oneHourFromNow
    }).save();
    
    console.log('Beispielaufgaben erstellt.');
    
    // Cron-Jobs starten
    require('./cronJobs');
});

client.on('message', async msg => {
    console.log('Nachricht erhalten:', msg.body);
    const Message = require('./models/message');

    // Nachricht speichern
    await new Message({
        from: msg.from,
        body: msg.body
    }).save();

    // Befehlsverarbeitung
    if (msg.body.startsWith('!auftrag')) {
        const parts = msg.body.split(' ');
        if (parts.length > 1) {
            const description = parts.slice(1).join(' ');
            const Order = require('./models/order');
            await new Order({
                from: msg.from,
                description: description
            }).save();
            client.sendMessage(msg.from, 'Dein Auftrag wurde erstellt: ' + description);
        } else {
            client.sendMessage(msg.from, 'Bitte gib eine Beschreibung für den Auftrag an.');
        }
    } else if (msg.body === '!aufträge') {
        const Order = require('./models/order');
        const orders = await Order.find({ from: msg.from });
        if (orders.length > 0) {
            let response = 'Deine Aufträge:\n';
            orders.forEach(order => {
                response += `- ${order.description} (Status: ${order.status})\n`;
            });
            client.sendMessage(msg.from, response);
        } else {
            client.sendMessage(msg.from, 'Du hast keine offenen Aufträge.');
        }
    } else {
        client.sendMessage(msg.from, 'Danke für deine Nachricht!');
    }
});

client.on('auth_failure', msg => { console.error('Authentifizierungsfe