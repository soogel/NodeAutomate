const Task = require('../models/task');
const Message = require('../models/message');

async function executeSyncTask(task) {
    const chats = await client.getChats();
    for (const chat of chats) {
        const messages = await chat.fetchMessages({ limit: 100 });
        for (const message of messages) {
            const receivedMessage = new Message({
                from: message.from,
                body: message.body,
                timestamp: message.timestamp
            });
            await receivedMessage.save();
        }
    }
    task.status = 'completed';
    await task.save();
}

module.exports = executeSyncTask;
