const Task = require('../models/task');

async function executeMessageTask(task) {
    const { to, text } = task.data;
    await client.sendMessage(to, text);
    task.status = 'completed';
    await task.save();
}

module.exports = executeMessageTask;
