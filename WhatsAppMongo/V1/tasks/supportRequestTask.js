const Task = require('../models/task');

async function executeSupportRequestTask(task) {
    const { to, requestDetails } = task.data;
    await client.sendMessage(to, `Deine Support-Anfrage wurde empfangen: ${requestDetails}`);
    task.status = 'completed';
    await task.save();
}

module.exports = executeSupportRequestTask;
