const Task = require('../models/task');

async function executeSurveyTask(task) {
    // Beispielhafte Umfrageerstellung
    await client.sendMessage('1234567890@c.us', 'Umfrage gestartet!');
    task.status = 'completed';
    await task.save();
}

module.exports = executeSurveyTask;

