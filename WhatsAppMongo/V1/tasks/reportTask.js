const Task = require('../models/task');

async function executeReportTask(task) {
    // Beispielhafte Berichtserstellung
    await client.sendMessage('1234567890@c.us', 'Bericht wurde erstellt!');
    task.status = 'completed';
    await task.save();
}

module.exports = executeReportTask;
