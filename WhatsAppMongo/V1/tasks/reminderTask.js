const Task = require('../models/task');

async function executeReminderTask(task) {
    const { to, text } = task.data;
    await client.sendMessage(to, text);
    task.status = 'completed';
    await task.save();
}

module.exports = executeReminderTask;

