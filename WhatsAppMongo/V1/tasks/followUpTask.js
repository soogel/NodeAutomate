const Task = require('../models/task');
const Message = require('../models/message');

async function executeFollowUpTask(task) {
    const { chatId } = task.data;
    const chat = await client.getChatById(chatId);
    const messages = await chat.fetchMessages({ limit: 100 });
    for (const message of messages) {
        const receivedMessage = new Message({
            from: message.from,
            body: message.body,
            timestamp: message.timestamp
        });
        await receivedMessage.save();
    }
    task.status = 'completed';
    await task.save();
}

module.exports = executeFollowUpTask;
