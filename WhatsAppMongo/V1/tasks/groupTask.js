const Task = require('../models/task');

async function executeGroupTask(task) {
    const { name, participants } = task.data;
    const group = await client.createGroup(name, participants);
    await client.sendMessage(group.id._serialized, 'Gruppe erstellt!');
    task.status = 'completed';
    await task.save();
}

module.exports = executeGroupTask;
