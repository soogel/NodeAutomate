const mongoose = require('mongoose');

const taskSchema = new mongoose.Schema({
    type: String, // Typ der Aufgabe: 'follow-up', 'message', 'group', 'sync', 'reminder', 'support-request', 'data-sync', 'report', 'survey'
    data: mongoose.Schema.Types.Mixed, // Daten, die für die Aufgabe benötigt werden
    status: { type: String, default: 'pending' },
    createdAt: { type: Date, default: Date.now },
    scheduledAt: Date // Geplantes Datum/Zeit für die Ausführung der Aufgabe
});

module.exports = mongoose.model('Task', taskSchema);
