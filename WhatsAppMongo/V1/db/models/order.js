const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({
    from: String,
    description: String,
    status: { type: String, default: 'pending' },
    createdAt: { type: Date, default: Date.now }
});

module.exports = mongoose.model('Order', orderSchema);