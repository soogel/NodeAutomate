
// sessionModel.js
const mongoose = require('mongoose');

const SessionSchema = new mongoose.Schema({
    sessionId: String,
    sessionData: Object,
});

module.exports = mongoose.model('Session', SessionSchema);