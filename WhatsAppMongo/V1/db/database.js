const mongoose = require('mongoose');

function connectDatabase() {
    return mongoose.connect('mongodb+srv://<username>:<password>@cluster0.mongodb.net/whatsappbot?retryWrites=true&w=majority', {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    });
}

module.exports = connectDatabase;
