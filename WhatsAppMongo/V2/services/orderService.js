const Order = require('../models/Order');

async function createOrder(from, description) {
    const order = new Order({ from, description });
    await order.save();
}

async function getOrders(from) {
    return await Order.find({ from });
}

module.exports = {
    createOrder,
    getOrders
};
