const Message = require('../models/Message');

async function saveMessage(from, body) {
    const message = new Message({ from, body });
    await message.save();
}

async function fetchMessages(chat) {
    const messages = await chat.fetchMessages({ limit: 100 });
    for (const message of messages) {
        await saveMessage(message.from, message.body);
    }
}

module.exports = {
    saveMessage,
    fetchMessages
};
