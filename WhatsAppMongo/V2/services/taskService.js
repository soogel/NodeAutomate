const Task = require('../models/Task');
const Message = require('../models/Message');

async function executeTasks(client) {
    const now = new Date();
    const tasks = await Task.find({
        scheduledAt: { $lte: now },
        status: 'pending'
    });

    for (const task of tasks) {
        if (task.type === 'message') {
            await client.sendMessage(task.data.to, task.data.text);
            task.status = 'completed';
            await task.save();
        } else if (task.type === 'reminder') {
            await client.sendMessage(task.data.to, task.data.text);
            task.status = 'completed';
            await task.save();
        }
        // Weitere Task-Typen hier hinzufügen
    }
}

async function createMessageTask(to, text, scheduledAt) {
    const task = new Task({
        type: 'message',
        data: { to, text },
        scheduledAt
    });
    await task.save();
}

async function getTasks() {
    var tasks = await Task.find(
        {status: 'pending'}
    );
    return tasks;
}

module.exports = {
    executeTasks,
    createMessageTask,
    getTasks
    // Weitere Exportfunktionen für Task-Typen
};
