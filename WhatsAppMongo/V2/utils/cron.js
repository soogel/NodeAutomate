const cron = require('node-cron');
const { executeTasks } = require('../services/taskService');

function setupCronJobs(client) {
    cron.schedule('* * * * *', () => {
        executeTasks(client);
    });
}

module.exports = {
    setupCronJobs
};
