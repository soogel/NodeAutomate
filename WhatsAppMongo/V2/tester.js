const { createOrder, getOrders } = require('./services/orderService');
const { saveMessage } = require('./services/messageService');
const { createMessageTask, getTasks } = require('./services/taskService');

const { initializeDatabase } = require('./config/db');

async function main(){
    // Datenbank initialisieren und Bot starten
await initializeDatabase();

var tasks = await getTasks({status: 'pending'});

for (const task of tasks) {
    console.log(JSON.stringify(task.scheduledAt) ,task.data.text  );
}



//console.log(JSON.stringify(tasks));
}

main();