const mongoose = require('mongoose');

async function initializeDatabase() {
    try {
        await mongoose.connect('mongodb://192.168.1.82:27017/whatsappbot', {
            useNewUrlParser: true,
            useUnifiedTopology: true,
        });
        console.log('Erfolgreich mit MongoDB verbunden');
    } catch (err) {
        console.error('Fehler bei der Verbindung mit MongoDB:', err);
        process.exit(1); // Bei Fehlern beenden
    }
}

module.exports = { initializeDatabase };
