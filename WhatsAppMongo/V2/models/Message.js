const mongoose = require('mongoose');

var messageSchema = new mongoose.Schema({
    from: String,
    body: String,
    timestamp: { type: Date, default: Date.now }
});

/* var Message = mongoose.model('Message', messageSchema);
module.exports = Message; */

module.exports =  mongoose.models.Message || mongoose.model('Message', messageSchema);