const mongoose = require('mongoose');

const taskSchema = new mongoose.Schema({
    type: String,
    data: mongoose.Schema.Types.Mixed,
    status: { type: String, default: 'pending' },
    createdAt: { type: Date, default: Date.now },
    scheduledAt: Date
});



module.exports =  mongoose.models.Task || mongoose.model('Task', taskSchema);
