const { Client, LocalAuth } = require('whatsapp-web.js');
const qrcode = require('qrcode-terminal');
const { initializeDatabase } = require('./config/db');
const { handleReady } = require('./controllers/botController');
const { setupCronJobs } = require('./utils/cron');

const client = new Client({
    authStrategy: new LocalAuth({ clientId: "client-one", dataPath: "./.wwebjs_auth" }),
    puppeteer: { headless: true }
});

// QR-Code generieren
client.on('qr', (qr) => {
    qrcode.generate(qr, { small: true });
});

// Client bereit
client.on('ready', async () => {
    await handleReady(client);
});

// Nachricht empfangen und verarbeiten
client.on('message', async msg => {
    const { handleMessage } = require('./controllers/botController');
    await handleMessage(client, msg);
});

// Datenbank initialisieren und Bot starten
initializeDatabase().then(() => {
    client.initialize();
    setupCronJobs(client);
});
