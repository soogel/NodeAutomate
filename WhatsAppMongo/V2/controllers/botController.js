const { createOrder, getOrders } = require('../services/orderService');
const { saveMessage } = require('../services/messageService');
const { createMessageTask, getTasks } = require('../services/taskService');

async function handleReady(client) {
    console.log('Client ist bereit!');

    const fiveHoursFromNow = new Date(Date.now() + 5 * 60 * 60 * 1000);
    const twoMinutesFromNow = new Date(Date.now() +  2 * 60 * 1000);
    await createMessageTask('4915234374843@c.us', 'Dies ist eine geplante Nachricht.', twoMinutesFromNow);
}

async function handleMessage(client, msg) {
    console.log('Nachricht erhalten:', msg.body);
    await saveMessage(msg.from, msg.body);

    if (msg.body.startsWith('!auftrag')) {
        const parts = msg.body.split(' ');
        if (parts.length > 1) {
            const description = parts.slice(1).join(' ');
            await createOrder(msg.from, description);
            client.sendMessage(msg.from, 'Dein Auftrag wurde erstellt: ' + description);
        } else {
            client.sendMessage(msg.from, 'Bitte gib eine Beschreibung für den Auftrag an.');
        }
    } else if (msg.body === '!aufträge') {
        const orders = await getOrders(msg.from);
        if (orders.length > 0) {
            let response = 'Deine Aufträge:\n';
            orders.forEach(order => {
                response += `- ${order.description} (Status: ${order.status})\n`;
            });
            client.sendMessage(msg.from, response);
        } else {
            client.sendMessage(msg.from, 'Du hast keine offenen Aufträge.');
        }
    }
    else if (msg.body === '!tasks') {
        //show tasks list
        console.log('doTasks');
        var tasks = await getTasks();
        //console.log(JSON.stringify(tasks));

        if (tasks.length > 0) {
            let response = 'Deine Aufträge:\n';
            tasks.forEach(task => {
                response += `- ${task.scheduledAt} (Decsription: ${task.data.text })\n`;
            });
            client.sendMessage(msg.from, response);
        } else {
            client.sendMessage(msg.from, 'Du hast keine offenen Aufträge.');
        }
    }
    else if (msg.body === 'info'){
        let response = 'Übersicht Befehle:\n';
        
        response += '!auftrag \n';
        response += '!aufträge \n';
        response += '!tasks\n';
        client.sendMessage(msg.from, response);
    }

}

module.exports = {
    handleReady,
    handleMessage
};


// for (const task of tasks) {
//     console.log(JSON.stringify(task.scheduledAt) ,task.data.text  );
// }