const { Client, LocalAuth } = require('whatsapp-web.js');
const mongoose = require('mongoose');
const qrcode = require('qrcode-terminal');

// Verbindung zur MongoDB herstellen
mongoose.connect('mongodb://192.168.1.82:27017/whatsappbot', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
}).then(() => {
    console.log('Erfolgreich mit MongoDB verbunden');
}).catch(err => {
    console.error('Fehler bei der Verbindung mit MongoDB:', err);
});

// Definiere ein Mongoose-Schema für Nachrichten
const messageSchema = new mongoose.Schema({
    from: String,
    body: String,
    timestamp: { type: Date, default: Date.now }
});

const Message = mongoose.model('Message', messageSchema);

// WhatsApp-Web Client initialisieren
const client = new Client({
    authStrategy: new LocalAuth({ clientId: "client-one", dataPath: "./.wwebjs_auth" }),
    puppeteer: { headless: true }
});

// QR-Code generieren
client.on('qr', (qr) => {
    qrcode.generate(qr, { small: true });
});

// Client bereit
client.on('ready', async () => {
    console.log('Client ist bereit!');

    // Hol alle Chats und speichere alte Nachrichten
    const chats = await client.getChats();
    for (const chat of chats) {
        const messages = await chat.fetchMessages({ limit: 100 }); // Die letzten 100 Nachrichten abrufen
        for (const message of messages) {
            // Speichere jede Nachricht in der Datenbank
            const receivedMessage = new Message({
                from: message.from,
                body: message.body,
                timestamp: message.timestamp
            });
            await receivedMessage.save();
        }
    }
});

// Authentifizierungsfehler behandeln
client.on('auth_failure', msg => {
    console.error('Authentifizierungsfehler:', msg);
});

// Nachricht empfangen und in der Datenbank speichern
client.on('message', async msg => {
    console.log('Nachricht erhalten:', msg.body);

    // Nachricht in der MongoDB speichern
    const receivedMessage = new Message({
        from: msg.from,
        body: msg.body
    });
    await receivedMessage.save();
    console.log('Nachricht in der Datenbank gespeichert.');

    // Beispielhafte Antwort
    if (msg.body.toLowerCase() === 'hallo') {
        client.sendMessage(msg.from, 'Hallo! Wie kann ich dir helfen?');
    } else {
        client.sendMessage(msg.from, 'Danke für deine Nachricht!');
    }
});

// Client initialisieren und starten
client.initialize();