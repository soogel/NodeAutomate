const { Client, RemoteAuth  } = require('whatsapp-web.js');
const mongoose = require('mongoose');
const qrcode = require('qrcode-terminal');
const { MongoStore } = require('wwebjs-mongo');

// MongoDB Verbindung
mongoose.connect('mongodb://192.168.1.82:27017/whatsappbot', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
});

// MongoStore für WhatsApp-Sitzungen
const store = new MongoStore({ mongoose: mongoose });

// Definiere ein Mongoose-Schema für empfangene Nachrichten
const messageSchema = new mongoose.Schema({
    from: String,
    body: String,
    timestamp: { type: Date, default: Date.now }
});

const Message = mongoose.model('Message', messageSchema);

// WhatsApp Web Client mit MongoStore initialisieren
const client = new Client({
    authStrategy: new RemoteAuth({
        store: store,
        clientId: 'yourSessionName',
        backupSyncIntervalMs: 300000
    })
});

client.on('qr', (qr) => {
    // QR-Code wird in der Konsole angezeigt
    qrcode.generate(qr, { small: true });
});

client.on('ready', () => {
    console.log('Client ist bereit!');
});

client.on('authenticated', () => {
    console.log('Authentifizierung erfolgreich!');
});

client.on('auth_failure', msg => {
    console.error('Authentifizierungsfehler:', msg);
});

client.on('message', async msg => {
    console.log('Nachricht erhalten:', msg.body);

    // Nachricht in der MongoDB speichern
    const receivedMessage = new Message({
        from: msg.from,
        body: msg.body
    });
    await receivedMessage.save();

    // Nachrichtenverarbeitung basierend auf Schlüsselwörtern
    if (msg.body.toLowerCase() === 'hallo') {
        client.sendMessage(msg.from, 'Hallo! Wie kann ich dir helfen?');
    } else if (msg.body.toLowerCase() === 'hilfe') {
        client.sendMessage(msg.from, 'Hier sind einige Befehle, die du verwenden kannst:\n- "Hallo" für eine Begrüßung\n- "Info" für Informationen\n- "Hilfe" für Unterstützung');
    } else if (msg.body.toLowerCase() === 'info') {
        client.sendMessage(msg.from, 'Ich bin ein einfacher WhatsApp-Bot, der mit Node.js und MongoDB erstellt wurde!');
    } else {
        client.sendMessage(msg.from, 'Entschuldigung, ich verstehe das nicht. Sende "Hilfe" für eine Liste der verfügbaren Befehle.');
    }
});

client.on('remote_session_saved', () => {
   console.log('Remote session saved');
});

// Client starten
client.initialize();