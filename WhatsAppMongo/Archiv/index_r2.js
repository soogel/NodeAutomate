const { Client, LegacySessionAuth } = require('whatsapp-web.js');
const mongoose = require('mongoose');
const qrcode = require('qrcode-terminal');
const Session = require('./db/sessionModel');

// MongoDB Verbindung einrichten
mongoose.connect('mongodb://192.168.1.82:27017/whatsappbot', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
});

const MessageSchema = new mongoose.Schema({
    from: String,
    body: String,
    timestamp: Date,
});

const Message = mongoose.model('Message', MessageSchema);

// Benutzerdefinierte Authentifizierungsstrategie für MongoDB
class MongoAuth extends LegacySessionAuth {
    constructor(sessionId) {
        super();
        this.sessionId = sessionId;
    }

    async init() {
        const session = await Session.findOne({ sessionId: this.sessionId });
        if (session && session.sessionData) {
            this.useLegacyAuth(session.sessionData);
        }
    }

    async saveSession(sessionData) {
        await Session.findOneAndUpdate(
            { sessionId: this.sessionId },
            { sessionData },
            { upsert: true }
        );
    }

    async removeSession() {
        await Session.deleteOne({ sessionId: this.sessionId });
    }
}

// WhatsApp Web Client initialisieren
const sessionId = 'user-session';
const client = new Client({
    authStrategy: new MongoAuth(sessionId),
});

client.on('qr', (qr) => {
    // QR-Code wird in der Konsole angezeigt
    qrcode.generate(qr, { small: true });
});

client.on('ready', () => {
    console.log('Client ist bereit!');
});

client.on('authenticated', (sessionData) => {
    console.log('Authentifiziert!', sessionData);
});

client.on('auth_failure', (msg) => {
    console.error('Authentifizierungsfehler', msg);
});

client.on('message', async msg => {
    console.log('Nachricht erhalten:', msg.body);

    // Nachricht verarbeiten oder speichern (wie in vorherigem Beispiel)
    if (msg.body.toLowerCase() === 'hallo') {
        client.sendMessage(msg.from, 'Hallo! Wie kann ich dir helfen?');
    } else {
        client.sendMessage(msg.from, 'Danke für deine Nachricht!');
    }
});

// Client starten
client.initialize();
