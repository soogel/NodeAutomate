const { Client, LocalAuth } = require('whatsapp-web.js');
const mongoose = require('mongoose');
const qrcode = require('qrcode-terminal');
const cron = require('node-cron');

// Verbindung zur MongoDB herstellen
mongoose.connect('mongodb://192.168.1.82:27017/whatsappbot', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
}).then(() => {
    console.log('Erfolgreich mit MongoDB verbunden');
}).catch(err => {
    console.error('Fehler bei der Verbindung mit MongoDB:', err);
});

// Definiere ein Mongoose-Schema für Nachrichten
const messageSchema = new mongoose.Schema({
    from: String,
    body: String,
    timestamp: { type: Date, default: Date.now }
});

const Message = mongoose.model('Message', messageSchema);

// Definiere ein Mongoose-Schema für Aufträge
const orderSchema = new mongoose.Schema({
    from: String,
    description: String,
    status: { type: String, default: 'pending' },
    createdAt: { type: Date, default: Date.now }
});

const Order = mongoose.model('Order', orderSchema);

// Definiere ein Mongoose-Schema für Aufgaben (Tasks)
const taskSchema = new mongoose.Schema({
    type: String, // Typ der Aufgabe: 'follow-up', 'message', 'group', 'sync'
    data: mongoose.Schema.Types.Mixed, // Daten, die für die Aufgabe benötigt werden
    status: { type: String, default: 'pending' },
    createdAt: { type: Date, default: Date.now },
    scheduledAt: Date // Geplantes Datum/Zeit für die Ausführung der Aufgabe
});

const Task = mongoose.model('Task', taskSchema);

// WhatsApp-Web Client initialisieren
const client = new Client({
    authStrategy: new LocalAuth({ clientId: "client-one", dataPath: "./.wwebjs_auth" }),
    puppeteer: { headless: true }
});

// QR-Code generieren
client.on('qr', (qr) => {
    qrcode.generate(qr, { small: true });
});

// Client bereit
client.on('ready', async () => {
    console.log('Client ist bereit!');

    // Hol alle Chats und speichere alte Nachrichten
    const chats = await client.getChats();
    for (const chat of chats) {
        const messages = await chat.fetchMessages({ limit: 100 }); // Die letzten 100 Nachrichten abrufen
        for (const message of messages) {
            // Speichere jede Nachricht in der Datenbank
            const receivedMessage = new Message({
                from: message.from,
                body: message.body,
                timestamp: message.timestamp
            });
            await receivedMessage.save();
        }
    }

    // Überprüfe und führe alle Aufgaben aus
    await executeTasks();
});

// Authentifizierungsfehler behandeln
client.on('auth_failure', msg => {
    console.error('Authentifizierungsfehler:', msg);
});

// Nachricht empfangen und verarbeiten
client.on('message', async msg => {
    console.log('Nachricht erhalten:', msg.body);

    // Nachricht in der MongoDB speichern
    const receivedMessage = new Message({
        from: msg.from,
        body: msg.body
    });
    await receivedMessage.save();
    console.log('Nachricht in der Datenbank gespeichert.');

    // Befehlsverarbeitung
    if (msg.body.startsWith('!auftrag')) {
        const parts = msg.body.split(' ');
        if (parts.length > 1) {
            const description = parts.slice(1).join(' ');

            // Erstelle einen neuen Auftrag
            const newOrder = new Order({
                from: msg.from,
                description: description
            });
            await newOrder.save();
            client.sendMessage(msg.from, 'Dein Auftrag wurde erstellt: ' + description);
        } else {
            client.sendMessage(msg.from, 'Bitte gib eine Beschreibung für den Auftrag an.');
        }
    } else if (msg.body === '!aufträge') {
        // Hol alle Aufträge des Benutzers
        const orders = await Order.find({ from: msg.from });
        if (orders.length > 0) {
            let response = 'Deine Aufträge:\n';
            orders.forEach(order => {
                response += `- ${order.description} (Status: ${order.status})\n`;
            });
            client.sendMessage(msg.from, response);
        } else {
            client.sendMessage(msg.from, 'Du hast keine offenen Aufträge.');
        }
    } else {
        // Beispielhafte Antwort
        if (msg.body.toLowerCase() === 'hallo') {
            client.sendMessage(msg.from, 'Hallo! Wie kann ich dir helfen?');
        } else {
            client.sendMessage(msg.from, 'Danke für deine Nachricht!');
        }
    }
});

// Funktion zum Ausführen von Aufgaben
async function executeTasks() {
    const now = new Date();
    const tasks = await Task.find({
        scheduledAt: { $lte: now },
        status: 'pending'
    });
    for (const task of tasks) {
        if (task.type === 'follow-up') {
            const { chatId } = task.data;
            // Hol die Chat-Nachrichten nach und speichere sie in der Datenbank
            const chat = await client.getChatById(chatId);
            const messages = await chat.fetchMessages({ limit: 100 }); // Die letzten 100 Nachrichten abrufen
            for (const message of messages) {
                const receivedMessage = new Message({
                    from: message.from,
                    body: message.body,
                    timestamp: message.timestamp
                });
                await receivedMessage.save();
            }
            // Markiere die Aufgabe als abgeschlossen
            task.status = 'completed';
            await task.save();
        } else if (task.type === 'message') {
            const { to, text } = task.data;
            // Sende eine Nachricht an die angegebene Nummer
            client.sendMessage(to, text);
            // Markiere die Aufgabe als abgeschlossen
            task.status = 'completed';
            await task.save();
        } else if (task.type === 'group') {
            const { name, participants } = task.data;
            // Erstelle eine Gruppe
            const group = await client.createGroup(name, participants);
            client.sendMessage(group.id._serialized, 'Gruppe erstellt!');
            // Markiere die Aufgabe als abgeschlossen
            task.status = 'completed';
            await task.save();
        } else if (task.type === 'sync') {
            // Synchronisiere alle Nachrichten
            const chats = await client.getChats();
            for (const chat of chats) {
                const messages = await chat.fetchMessages({ limit: 100 }); // Die letzten 100 Nachrichten abrufen
                for (const message of messages) {
                    const receivedMessage = new Message({
                        from: message.from,
                        body: message.body,
                        timestamp: message.timestamp
                    });
                    await receivedMessage.save();
                }
            }
            // Markiere die Aufgabe als abgeschlossen
            task.status = 'completed';
            await task.save();
        }
    }
}

// Regelmäßige Überprüfung auf auszuführende Aufgaben (z.B. jede Minute)
cron.schedule('* * * * *', () => {
    executeTasks();
});

// Client initialisieren und starten
client.initialize();
