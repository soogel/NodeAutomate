const { Client, LocalAuth } = require('whatsapp-web.js');
const mongoose = require('mongoose');
const qrcode = require('qrcode-terminal');

// MongoDB Verbindung einrichten
mongoose.connect('mongodb://192.168.1.82:27017/whatsappbot', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
});

const MessageSchema = new mongoose.Schema({
    from: String,
    body: String,
    timestamp: Date,
});

const Message = mongoose.model('Message', MessageSchema);

// WhatsApp Web Client einrichten
const client = new Client({
    authStrategy: new LocalAuth() // Speichert die Session, damit du dich nicht jedes Mal neu anmelden musst
});

client.on('qr', (qr) => {
    // QR-Code wird in der Konsole angezeigt
    qrcode.generate(qr, { small: true });
});

client.on('ready', () => {
    console.log('Client ist bereit!');
});

client.on('message', async msg => {
    console.log('Nachricht erhalten:', msg.body);

    // Nachricht in der MongoDB speichern
    const newMessage = new Message({
        from: msg.from,
        body: msg.body,
        timestamp: new Date(),
    });
    
    await newMessage.save();

    // Eine automatische Antwort senden
    if (msg.body.toLowerCase() === 'hallo') {
        client.sendMessage(msg.from, 'Hallo! Wie kann ich dir helfen?');
    } else {
        client.sendMessage(msg.from, 'Danke für deine Nachricht!');
    }
});

// Client starten
client.initialize();
