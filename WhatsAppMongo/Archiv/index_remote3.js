const { Client } = require('whatsapp-web.js');
const mongoose = require('mongoose');
const qrcode = require('qrcode-terminal');
const { MongoStore } = require('wwebjs-mongo');

// Verbinde dich mit MongoDB Atlas oder einem anderen Remote-MongoDB-Server
mongoose.connect('mongodb://192.168.1.82:27017/whatsappbot', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
}).then(() => {
    console.log('Erfolgreich mit MongoDB verbunden');
}).catch(err => {
    console.error('Fehler bei der Verbindung mit MongoDB:', err);
});

// MongoStore für WhatsApp-Sitzungen
const store = new MongoStore({ mongoose: mongoose });

// WhatsApp Web Client initialisieren
const client = new Client({
    authStrategy: store, // MongoStore als Authentifizierungsstrategie verwenden
});

client.on('qr', (qr) => {
    // QR-Code wird in der Konsole angezeigt
    qrcode.generate(qr, { small: true });
});

client.on('ready', () => {
    console.log('Client ist bereit!');
});

client.on('authenticated', () => {
    console.log('Authentifizierung erfolgreich!');
});

client.on('auth_failure', msg => {
    console.error('Authentifizierungsfehler:', msg);
});

client.on('message', async msg => {
    console.log('Nachricht erhalten:', msg.body);

    // Beispiel für eine automatische Antwort
    if (msg.body.toLowerCase() === 'hallo') {
        client.sendMessage(msg.from, 'Hallo! Wie kann ich dir helfen?');
    } else {
        client.sendMessage(msg.from, 'Danke für deine Nachricht!');
    }
});

// Client starten
client.initialize();
