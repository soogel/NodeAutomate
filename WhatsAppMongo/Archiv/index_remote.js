const { Client, RemoteAuth } = require('whatsapp-web.js');

// Require database
const { MongoStore } = require('wwebjs-mongo');
const mongoose = require('mongoose');

//vars
var mongoDB = 'mongodb://192.168.1.82:27017/whatsappbot';

// Load the session data
mongoose.connect(mongoDB).then(() => {
    console.log('1');
    const store = new MongoStore({ mongoose: mongoose });
    console.log('2');
    const client = new Client({
        puppeteer: {
					args: ['--no-sandbox', '--disable-setuid-sandbox'],
				},
        authStrategy: new RemoteAuth({
            store: store,
         backupSyncIntervalMs: 60000
        })
    });
    console.log('3');
    client.on('loading_screen', (percent, message) => {
        console.log('LOADING SCREEN', percent, message);
    });

    client.on('remote_session_saved', () => {
        console.log('Remote session saved');
     });
     client.on('qr', (qr) => {
        // QR-Code wird in der Konsole angezeigt
        qrcode.generate(qr, { small: true });
    });
    
    client.on('ready', () => {
        console.log('Client ist bereit!');
    });
    
    client.on('authenticated', () => {
        console.log('Authentifizierung erfolgreich!');
    });
    
    client.on('auth_failure', msg => {
        console.error('Authentifizierungsfehler:', msg);
    });
    
    client.initialize();
});