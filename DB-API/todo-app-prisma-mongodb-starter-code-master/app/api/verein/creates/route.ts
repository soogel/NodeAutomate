// app/api/todo/creates/route.ts

import { db } from "@/lib/db";

import { NextResponse } from "next/server";

//for multiple Inserts
export async function POST(req: Request) {
  try {

    // detsrtucture todoTitle from the incoming request
    //const { acronym, alias, category, city, country, location, logo , organization, partyID, postalCode,search_term,state  } = await req.json(); 
    const respond = await req.json(); 

    if (!(respond.length > 0)) {
      return new NextResponse("partyID required", { status: 400 });
    }

    //Data cleanup
    //not needed anymore done before
    console.log(respond);

    // Create and save todo on the database
    const todo = await db.reweverein.createMany({
      data: respond,
    });

    //return NextResponse.json(todo, { status: 200 }); // Respond with the created todo
    return NextResponse.json({done: 'done'}, { status: 200 }); // Respond with the created todo
  } catch (error) {
    console.log("[POST TODO]", error);
    return new NextResponse("Internal Server Error", { status: 500 }); // Handle errors
  }
}