// app/api/todo/route.ts

import { db } from "@/lib/db";
import { NextResponse } from "next/server";

export async function GET() {
 
 
 //get partyID 

 //get checklast Scann
 const date = new Date();
 getFullYear()
 getMonth()
 getDate()
 
  try {
    const plzCount = await db.plz.count();
    const skip = Math.floor(Math.random() * plzCount);
    //fetch todos from the db
    const todos = await db.scheine.findMany({
      take: 1,
      skip: skip,
      where: {isCompleted: false, },      
      orderBy: {
        zipcode: "desc",
      },
    });

   

    // respond with the todos
    return NextResponse.json(todos, { status: 200 }); 
  } catch (error) {
    console.log("[GET PLZ]", error);

// Handle errors
    return new NextResponse("Internal Server Error", { status: 500 }); 
  }
}