// app/api/todo/create/route.ts

import { db } from "@/lib/db";

import { NextResponse } from "next/server";

export async function POST(req: Request) {
  //get date and convert
  let yourDate = new Date();
  var yourdate2 = yourDate.toISOString().split("T")[0];
  try {
    // detsrtucture todoTitle from the incoming request
    const vereinsscheinObj = await req.json();
    //console.log(vereinsscheinObj);

    //cast data
    var partyID = vereinsscheinObj.organizationPartyId;
    var organizationPartyId = vereinsscheinObj.organizationPartyId;
    var partyClubDescription = vereinsscheinObj.partyClubDescription;
    var totalBalance = vereinsscheinObj.totalBalance;
    var availableBalance = vereinsscheinObj.availableBalance;
    var redeemed = vereinsscheinObj.redeemed;
    var Customer_Registered = vereinsscheinObj.Customer_Registered;
    var wishlistArray = vereinsscheinObj.WishList; //Array of wishlist
    var disabled = vereinsscheinObj.disabled;

    //adressdata
    if(vereinsscheinObj.address.length < 1){
      vereinsscheinObj.address = {
        "countryGeoId": "",
        "countryStr": "",
        "address": "",
        "city": "",
        "addressType": "",
        "clubName": "",
        "phoneNumberId": "",
        "addressId": "",
        "attnName": "",
        "phoneNumber": "",
        "regionStr": "",
        "toName": "",
        "schoolDistrictGeoId": "",
        "postCode": 0,
        "stateProvinceGeoId": ""
      }
    }
    var countryGeoId = vereinsscheinObj.address.countryGeoId;
    if(typeof String(countryGeoId) == "string") { countryGeoId = countryGeoId.toString(); }
    var countryStr = vereinsscheinObj.address.countryStr;
    var address = vereinsscheinObj.address.address;
    var city = vereinsscheinObj.address.city;
    var addressType = vereinsscheinObj.address.addressType;
    var clubName = vereinsscheinObj.address.clubName;
    var phoneNumberId = vereinsscheinObj.address.phoneNumberId;
    if(typeof String(phoneNumberId) == "string") { phoneNumberId = phoneNumberId.toString(); }

    var addressId = vereinsscheinObj.address.addressId;
    if(typeof String(addressId) == "string") { addressId = addressId.toString(); }

    var attnName = vereinsscheinObj.address.attnName;
    var phoneNumber = vereinsscheinObj.address.phoneNumber;
    if(typeof String(phoneNumber) == "string") { phoneNumber = phoneNumber.toString(); }

    var regionStr = vereinsscheinObj.address.regionStr;
    var toName = vereinsscheinObj.address.toName;
    var schoolDistrictGeoId = vereinsscheinObj.address.schoolDistrictGeoId;
    if(typeof String(schoolDistrictGeoId) == "string") { schoolDistrictGeoId = schoolDistrictGeoId.toString(); }
    var postCode = vereinsscheinObj.address.postCode;
    //Data cleanup
    postCode = postCode.toString();
    if (postCode.length < 5) {
      console.log("toshort");
      postCode = "0" + postCode;
    }

    var stateProvinceGeoId = vereinsscheinObj.address.stateProvinceGeoId;
    if(typeof String(stateProvinceGeoId) == "string") { stateProvinceGeoId = stateProvinceGeoId.toString(); }

    var disabled = vereinsscheinObj.disabled;
    var Customer_Registered = vereinsscheinObj.Customer_Registered;
    var accountName = vereinsscheinObj.accountName;

    if (!partyID ) {
      console.log('Daten nicht korrekt');
      console.log('partyID: ' + partyID);
      console.log('totalBalance: ' + totalBalance);
      return new NextResponse("PartyID required", { status: 400 });
    }

    //Scheine

    // Create and save todo on the database
    try {
       //Scheine 
    console.log('scheine');
      const todo = await db.scheine.create({
      data: {
        partyID: partyID,
        totalBalance:totalBalance,
        redeemed: redeemed,
        createdAt: new Date(yourdate2)
      },
    }); 
  }
  catch (err) {}

    //console.log("länge array #" + wishlistArray.length);

    //Wishlist --> map for each
    for (var i = 0; i < wishlistArray.length; i++) {
      wishlistArray[i].partyID = partyID;

      try {
         const wishlist = await db.wishlist.create({
          data: wishlistArray[i],
        }); 
      } catch (err) {}
    }
    // Create and save todo on the database

    
    try {
      //Scheine 2
    console.log('verein2');
      const vereine2 = await db.vereine2.create({
        data: {
          organizationPartyId: organizationPartyId,
          countryGeoId: countryGeoId,
          countryStr: countryStr,
          address: address,
          city: city,
          addressType: addressType,
          clubName: clubName,
          phoneNumberId: phoneNumberId,
          addressId: addressId,
          attnName: attnName,
          phoneNumber: phoneNumber,
          regionStr: regionStr,
          toName: toName,
          schoolDistrictGeoId: schoolDistrictGeoId,
          postCode: postCode,
          stateProvinceGeoId: stateProvinceGeoId,
          disabled: disabled,
          Customer_Registered: Customer_Registered,
          accountName: accountName,
          partyClubDescription: partyClubDescription,
          totalBalance: totalBalance,
        },
      });
      //console.log(vereine2);
    } catch (error) {
      console.log(error);
    }

    return NextResponse.json({ text: "hi" }, { status: 200 }); // Respond with the created todo
  } catch (error) {
    console.log("[POST Scheine]", error);
    return new NextResponse("Internal Server Error", { status: 500 }); // Handle errors
  }
}

//totalBalance : ScheineAnzahl
