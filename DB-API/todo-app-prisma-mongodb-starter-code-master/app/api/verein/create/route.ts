// app/api/todo/create/route.ts

import { db } from "@/lib/db";

import { NextResponse } from "next/server";

export async function POST(req: Request) {
  try {

    // detsrtucture todoTitle from the incoming request
    const { acronym, alias, category, city, country, location, logo , organization, partyID, postalCode,search_term,state  } = await req.json(); 

    if (!partyID) {
      return new NextResponse("partyID required", { status: 400 });
    }

    //Data cleanup
    var postalCode2 = postalCode.toString();
    if(postalCode2.length < 5) {console.log('toshort'); postalCode2 = '0' + postalCode2;}


    // Create and save todo on the database
    const todo = await db.reweverein.create({
      data: {
        acronym: acronym,
        alias: alias,
        category: category,
        city: city,
        country: country,
        location: location,
        logo: logo,
        organization: organization,
        partyID: partyID,
        postalCode: postalCode2,
        search_term: search_term,
        state: state
      },
    });

    return NextResponse.json(todo, { status: 200 }); // Respond with the created todo
  } catch (error) {
    console.log("[POST TODO]", error);
    return new NextResponse("Internal Server Error", { status: 500 }); // Handle errors
  }
}