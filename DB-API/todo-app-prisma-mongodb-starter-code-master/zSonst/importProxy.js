const fs = require("fs");
const proxys = require("./goodProxy2.json");

async function sendProxy(vereineData) {
  var data = "";
  vereinData = JSON.stringify(vereineData);
  var url = "http://localhost:3000/api/proxy/create";
  var config = {
    credentials: "include",
    headers: {
      "User-Agent":
        "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:126.0) Gecko/20100101 Firefox/126.0",
      Accept: "application/json, text/plain, */*",
      "Accept-Language": "de,en-US;q=0.7,en;q=0.3",
      "Sec-GPC": "1",
      "Sec-Fetch-Dest": "empty",
      "Sec-Fetch-Mode": "no-cors",
      "Sec-Fetch-Site": "same-site",
      Priority: "u=4",
      Pragma: "no-cache",
      "Cache-Control": "no-cache",
    },
    referrer: "http://localhost:3000/",
  };
  try {
    var respond = await axios.post(url, vereinData);
    data = await respond.data;
    //console.log(data);

    if (data > 0) {
      console.log("daten");
    }
  } catch (error) {
    if (error.response && error.response.status == 401) {
      console.log("Token not valid!");
    } else {
      console.log("error");

      //console.log(error.response);
      //console.log(error.response.status);
      if (error.response && error.response.status == 500) {
      }
    }
  }

  return data;
}


function cleanVereine(vereinObj) {
    vereinObj.postalCode = vereinObj.postalCode.toString();
    //if(vereinObj.postalCode.lenght < 5) {vereinObj.postalCode = '0' + vereinObj.postalCode;}
    if (vereinObj.postalCode.length < 5) {
      vereinObj.postalCode = "0" + vereinObj.postalCode;
    }
    //console.log(vereinObj);
    //console.log(vereinObj.postalCode.length + ' #' + vereinObj.postalCode +'#');
    return vereinObj;
  }

  function logData(data){
    console.log(data);
  }

  //console.log(proxys);

  async function sendProxyEinzel(Proxy) {
    var data='';
    var url = "http://localhost:3000/api/proxy/create";
    try{
     var response = await fetch(url, {
      method: "POST", // *GET, POST, PUT, DELETE, etc.
      mode: "cors", // no-cors, *cors, same-origin
      cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
      credentials: "same-origin", // include, *same-origin, omit
      headers: {
        "Content-Type": "application/json",
        // 'Content-Type': 'application/x-www-form-urlencoded',
      },
      redirect: "follow", // manual, *follow, error
      referrerPolicy: "no-referrer", // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
      body: JSON.stringify(Proxy), // body data type must match "Content-Type" header
    });
    //console.log(response);
    if(response.status === 200){
        data = response.json();
    }
   
  }
  catch (e){
  
  }
    return data;
  }

//proxys2.map(logData);



//proxys.map(sendProxyEinzel);

var proxyObj = {
    proxyurl: 'http://185.217.199.176:4444',
    protocol: 'http',
    host: '185.217.199.176',
    port: 4444
  }
  
  var test = sendProxyEinzel(proxyObj);
