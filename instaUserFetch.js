await fetch("http://localhost:8080/proxy/https://www.instagram.com/api/v1/users/web_profile_info/?username=purrpaws_fotografie", {
    "credentials": "include",
    "headers": {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/20100101 Firefox/117.0",
        "Accept": "*/*",
        "Accept-Language": "de,en-US;q=0.7,en;q=0.3",
        "X-Mid": "19d72r7cc62jx9zxp10zluzwhw1s3e11nbikpf1m5zriz4jzz3b",
        "X-CSRFToken": "sb2JsHtYChUF46WEYIrND0UTOXTocKFF",
        "X-IG-App-ID": "936619743392459",
        "X-ASBD-ID": "129477",
        "X-IG-WWW-Claim": "0",
        "X-Web-Device-Id": "7D11EF88-4665-4E68-A42F-2BFFDB0E1291",
        "Sec-Fetch-Dest": "empty",
        "Sec-Fetch-Mode": "cors",
        "Sec-Fetch-Site": "same-origin",
        "Sec-GPC": "1"
    },
    "referrer": "http://localhost:8080/proxy/https://www.instagram.com/purrpaws_fotografie/",
    "method": "GET",
    "mode": "cors"
});