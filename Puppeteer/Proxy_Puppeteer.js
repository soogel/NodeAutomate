// local setup
    const browser = await puppeteerExtra.launch({
      headless: false,
      args: [`--proxy-server=http://${ip}:${port}`],
      // devtools: true,
      executablePath:
        "/Applications/Google Chrome.app/Contents/MacOS/Google Chrome",
    });

    const page = await browser.newPage();

    await page.authenticate({
      username: "user",
      password: "pass",
    });