import fetch from "node-fetch";
import * as cheerio from "cheerio";

async function main(adId) {
  try {
    console.log(`adId: ${adId}`);
    const ad = await run(adId);
    console.log("ad: ", ad);
    return {
      statusCode: 200,
      body: JSON.stringify(ad),
    };
  } catch (error) {
    console.log(error);
    return { statusCode: 500, body: JSON.stringify(error) };
  }
}

const facebookMarkupWeDontWant = [
  "New feature: exact phrase search - use quotes around search terms to see results containing the same group of words in the same order",
  "New filter: media type - narrow your results by ads containing images, memes, video or transcripts of video.",
  " Get notified by email about Ad Library outages and feature announcements (you must be logged into your Facebook account to subscribe). ",
  "Keyboard shortcut help...",
  "Accessibility Help Center",
  "Submit feedback",
  "Updates from Facebook Accessibility",
  "Events",
  "Friend Requests",
  "Friends",
  "Groups",
  "Marketplace",
  "Messenger",
  "News Feed",
  "Notifications",
  "Pages",
  "Profile",
  "Settings",
  "Watch",
];

function getBody(text) {
  const markupIndex = text.indexOf(`"markup":[`);
  const elementsIndex = text.indexOf("elements");
  const markupString = text.substring(markupIndex + 9, elementsIndex - 2);
  const markupJson = JSON.parse(` { "markup": ${markupString}}`);
  for (let idx = 0; idx < markupJson?.markup.length; idx++) {
    const markupEl = markupJson?.markup[idx];
    const markupElObjHtml = markupEl?.[1]?.__html;
    const doWeWantTheText = !facebookMarkupWeDontWant.includes(markupElObjHtml);
    if (doWeWantTheText) {
      return markupElObjHtml;
    }
  }
}

function getAdData(text) {
  const $ = cheerio.load(text);
  const data = $("script");
  let returnValue = {};
  data.each((i, elem) => {
    if (elem.type === "script") {
      const data = elem?.children?.[0]?.data;
      if (data?.includes(`cards`)) {
        const elJs = elem?.children?.[0]?.data;
        const constructorIdx = elJs.indexOf("constructor");
        const constructorCurly = constructorIdx - 2;
        const nonBlockingPreloadersIdx = elJs.indexOf("nonBlockingPreloaders");
        let endIndex;
        for (let idx = nonBlockingPreloadersIdx; idx < elJs.length; idx++) {
          const element = elJs[idx];
          if (element === "}") {
            endIndex = idx;
            break;
          }
        }
        const jsonString = elJs.substring(constructorCurly, endIndex + 1);
        const json = JSON.parse(jsonString);
        const body = getBody(elJs);
        const deeplinkAdCard = json?.props?.deeplinkAdCard;
        returnValue = {
          ...deeplinkAdCard,
          snapshot: { ...deeplinkAdCard?.snapshot, body },
        };
        return;
      }
    }
  });
  return returnValue;
}

async function run(adId) {
  try {
    const res = await fetch(
      `https://www.facebook.com/ads/library/?id=${adId}`,
      {
        headers: {
          accept:
            "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
          "accept-language": "en-US,en-CA;q=0.9,en-AU;q=0.8,en;q=0.7",
          "cache-control": "max-age=0",
          "sec-ch-prefers-color-scheme": "dark",
          "sec-ch-ua":
            '" Not A;Brand";v="99", "Chromium";v="99", "Google Chrome";v="99"',
          "sec-ch-ua-mobile": "?0",
          "sec-ch-ua-platform": '"macOS"',
          "sec-fetch-dest": "document",
          "sec-fetch-mode": "navigate",
          "sec-fetch-site": "same-origin",
          "sec-fetch-user": "?1",
          "upgrade-insecure-requests": "1",
          "viewport-width": "1920",
          cookie:
            "datr=IaPXYUIn4RTC67O1BS-pLrR4; sb=JaPXYS85WTVaIiYE4STR9rSG; c_user=100009005165489; m_pixel_ratio=1; presence=C%7B%22t3%22%3A%5B%5D%2C%22utc3%22%3A1647888680204%2C%22v%22%3A1%7D; usida=eyJ2ZXIiOjEsImlkIjoiQXI5N2hheDEycTloNGMiLCJ0aW1lIjoxNjQ4MDUyMzE1fQ%3D%3D; xs=114%3AEyl-y9cSjgvWRQ%3A2%3A1644261031%3A-1%3A2121%3A%3AAcVCECnEQsuyvWAs1KpV99D9jtjMd94OaqOgRF5uF1vR; fr=0gHwSD9eNvtEP7FJp.AWXrTviaC1hP0TN3RVyuF6dXrRs.BiQgt-.bw.AAA.0.0.BiQgt-.AWUfd2WYFEA; wd=1920x469",
        },
        referrerPolicy: "origin-when-cross-origin",
        body: null,
        method: "GET",
      }
    );
    const text = await res.text();
    const adData = getAdData(text);
    console.log("adData: ", adData);
    return adData;
  } catch (error) {
    console.log("Error: " + error.message);
  }
}
// you need to run: npm install node-fetch cheerio
// replace adId with the ad id you want to scrape
const adId = "246984218506193";
main(adId);