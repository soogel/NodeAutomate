function genPassword(n, filter) {

  const variables = {
    uppercase:'ABCDEFGHIJKLMNOPQRSTUV',
    lowercase: 'abcdefghijklmnopqrstuvwxyz',
    nums: '0123456789',
    symbols: '!@#$%^&*?<>'

  }

  const filters = [];
  let password = '';

  for (key in filter) {
   if (filter[key]) {
    filters.push(key);
   }
  }

  filters.forEach(el => {
    let randomNum = Math.floor(Math.random() * variables[el].length);
    password += variables[el][randomNum];
  })

  while (password.length < n) {
      let randomFilter = Math.floor(Math.random() * filters.length);
     let filter = filters[randomFilter];
     let randomNum = Math.floor(Math.random() * variables[filter].length);

      password += variables[filter][randomNum];
  }
  return password;
}