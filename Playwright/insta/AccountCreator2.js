// Import the necessary modules
const { chromium } = require('playwright');

// Set up the browser context
async function main() {
  // Launch a new Chromium browser instance
  const browser = await chromium.launch();
  const context = await browser.newContext();

  // Create a new page object within the context
  const page = await context.newPage();

  // Navigate to Instagram.com
  await page.goto("https://www.instagram.com/");

  // Click on "Sign up" button
  await page.click('text="Sign up"');

  // Enter username, email, and password
  await page.fill('[name="username"]', 'your_username');
  await page.fill('[name="email"]', 'your_email@example.com');
  await page.fill('[name="password"]', 'your_password');

  // Click on "Next" button
  await page.click('text="Next"');

  // Wait for the account creation process to complete
  await page.waitForNavigation();

  // Print out the created account information (optional)
  console.log(await page.innerText('h2'));

  // Close the browser instance
  await browser.close();
}

main().then(() => {
  process.exit(0);
}).catch((error) => {
  console.error(error);
  process.exit(1);
});