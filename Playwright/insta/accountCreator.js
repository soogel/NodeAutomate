import { test, expect } from '@playwright/test';

test('test', async ({ page }) => {
  await page.goto('https://www.instagram.com/');
  await page.getByRole('button', { name: 'Optionale Cookies ablehnen' }).click();
  await page.getByRole('link', { name: 'Registrieren' }).click();
  await page.getByLabel('Handynummer oder E-Mail-Adresse').click();
  await page.getByLabel('Handynummer oder E-Mail-Adresse').click();
  await page.getByLabel('Handynummer oder E-Mail-Adresse').click();
  await page.getByLabel('Handynummer oder E-Mail-Adresse').fill('jennie.nichols');
  await page.getByLabel('Handynummer oder E-Mail-Adresse').click();
  await page.getByLabel('Handynummer oder E-Mail-Adresse').press('Control+a');
  await page.getByLabel('Handynummer oder E-Mail-Adresse').press('Control+c');
  await page.getByLabel('Handynummer oder E-Mail-Adresse').click();
  await page.getByLabel('Handynummer oder E-Mail-Adresse').press('Control+a');
  await page.getByLabel('Handynummer oder E-Mail-Adresse').fill('1xmailer1x+jennie.nichols@gmail.com');
  await page.getByLabel('Benutzername').click();
  await page.getByLabel('Benutzername').fill('yellowpeacock117');
  await page.getByLabel('Vollständiger Name').click();
  await page.getByLabel('Benutzername').click();
  await page.getByLabel('Benutzername').click();
  await page.getByLabel('Vollständiger Name').click();
  await page.getByLabel('Vollständiger Name').fill('Jennie Nichols');
  await page.getByLabel('Benutzername').click();
  await page.getByLabel('Benutzername').click();
  await page.getByLabel('Benutzername').click();
  await page.getByLabel('Passwort').click();
  await page.getByLabel('Passwort').click();
  await page.getByLabel('Passwort').fill('addison');
  await page.getByRole('main').locator('div').filter({ hasText: 'Registriere dich, um die Fotos und Videos deiner Freunde zu sehen.Mit Facebook a' }).first().click();
  await page.getByLabel('Benutzername').press('Control+a');
  await page.getByRole('main').locator('div').filter({ hasText: 'Registriere dich, um die Fotos und Videos deiner Freunde zu sehen.Mit Facebook a' }).first().click();
  await page.getByLabel('Passwort').click();
  await page.getByLabel('Passwort').press('Control+a');
  await page.getByLabel('Passwort').fill('Qwertzuio');
  await page.getByLabel('Passwort').press('Control+a');
  await page.getByLabel('Passwort').fill('Qwertzuiopü+');
  await page.getByRole('button', { name: 'Weiter' }).click();
  await page.getByLabel('Benutzername').click();
  await page.getByLabel('Benutzername').fill('yellowpeacock1177');
  await page.getByRole('main').locator('div').filter({ hasText: 'Registriere dich, um die Fotos und Videos deiner Freunde zu sehen.Mit Facebook a' }).first().click();
  await page.getByLabel('Benutzername').click();
  await page.getByLabel('Benutzername').press('Control+a');
  await page.getByLabel('Benutzername').fill('crazycat895');
  await page.getByText('Registriere dich, um die Fotos und Videos deiner Freunde zu sehen.Mit Facebook a').click();
  await page.getByLabel('Benutzername').click();
  await page.getByLabel('Benutzername').press('Control+a');
  await page.getByLabel('Benutzername').fill('smallpeacock243!');
  await page.getByRole('main').locator('div').filter({ hasText: 'Registriere dich, um die Fotos und Videos deiner Freunde zu sehen.Mit Facebook a' }).first().click();
  await page.getByText('Registriere dich, um die Fotos und Videos deiner Freunde zu sehen.Mit Facebook a').click();
  await page.getByLabel('Benutzername').click();
  await page.getByLabel('Benutzername').press('Control+a');
  await page.getByLabel('Benutzername').press('Control+c');
  await page.getByRole('main').locator('div').filter({ hasText: 'Registriere dich, um die Fotos und Videos deiner Freunde zu sehen.Mit Facebook a' }).first().click();
  await page.getByRole('button', { name: 'Weiter' }).click();
  await page.getByLabel('Benutzername').click();
  await page.getByLabel('Benutzername').fill('smallpeacock243__');
  await page.getByLabel('Benutzername').press('Control+a');
  await page.getByLabel('Benutzername').press('Control+c');
  await page.getByRole('button', { name: 'Weiter' }).click();
  await page.getByTitle('Tag:').selectOption('15');
  await page.getByTitle('Monat:').selectOption('7');
  await page.getByRole('button', { name: 'Weiter' }).click();
  await page.getByPlaceholder('Bestätigungscode').click();
  await page.getByPlaceholder('Bestätigungscode').fill('970365');
  await page.getByRole('button', { name: 'Weiter' }).click();
  await page.goto('https://www.instagram.com/');
  await page.getByLabel('Optionale Cookies ablehnen').click();
  await page.locator('._acan').first().click();
  await page.locator('div:nth-child(6) > div > div > div > div:nth-child(3) > div > ._acan').click();
  await page.locator('div:nth-child(8) > div > div > div > div:nth-child(3) > div > ._acan').click();
  await page.locator('div:nth-child(12) > div > div > div > div:nth-child(3) > div > ._acan').click();
  await page.getByRole('link', { name: 'Startseite Startseite' }).click();
  await page.getByRole('link', { name: 'Suche Suche' }).click();
  await page.getByLabel('Suchfeld leeren').click();
  await page.getByRole('link', { name: 'Entdecken' }).click();
  await page.getByRole('link', { name: 'Suche Suche' }).click();
  await page.getByPlaceholder('Suchen').fill('Hannover');
  await page.getByPlaceholder('Suchen').press('Enter');
  await page.getByLabel('Nicht personalisiert').click();
  await page.getByLabel('Für dich').click();
  await page.getByRole('link', { name: 'Entdecken' }).click();
  await page.getByRole('link', { name: 'smallpeacock243__s Profilbild Profil' }).click();
  await page.getByRole('button', { name: 'Füge ein Profilbild hinzu' }).click();
  await page.getByRole('button', { name: 'Füge ein Profilbild hinzu' }).setInputFiles('thispersondoesnotexist.jpeg');
  await page.getByRole('button', { name: 'Profilbild ändern' }).click();
  await page.getByRole('button', { name: 'Abbrechen' }).click();
  await page.getByRole('link', { name: 'Profil bearbeiten' }).click();
  await page.getByRole('button', { name: 'Schließen' }).click();
});

function getRandomUser() {  

    //https://randomuser.me/api/?gender=female
    //https://randomuser.me/api/?nat=de&gender=female
    $.ajax({
        url: 'https://randomuser.me/api/',
        dataType: 'json',
        success: function(data) {
          console.log(data);
        }
      });
      return 
}