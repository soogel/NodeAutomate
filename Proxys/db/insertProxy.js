//const connectMongo  = require('./connectMongo.cjs');

import { connectMongo, closeMongo } from './connectMongo.js'
import Proxys from './models/proxy.js';



 export default async function addProxy(proxysJSON) {
  try {
    console.log('CONNECTING TO MONGO');
    await connectMongo();
    console.log('CONNECTED TO MONGO');

    console.log(proxysJSON);

    console.log('CREATING DOCUMENT');
    const ProxysDocument = await Proxys.create(proxysJSON);
    console.log('CREATED DOCUMENT');
    await closeMongo();
    
  } catch (error) {
    console.log(error);
    //await closeMongo();
  }
} 