//const connectMongo  = require('./connectMongo.cjs');

import { connectMongo, closeMongo } from './connectMongo.js'
import Proxys from './models/proxy.js';



 export default async function getDBProxy(proxysJSON) {
  var ProxyDocument = '';
  try {
    console.log('CONNECTING TO MONGO');
    await connectMongo();
    console.log('CONNECTED TO MONGO');

    //console.log(proxysJSON);

    console.log('get DOCUMENT');
    //const ProxysDocument = await Proxys.create(proxysJSON);
     //ProxyDocument = await Proxys.findOne({});
     ProxyDocument = await Proxys.find({}).sort({ lastChecked: 1 }).limit(1);
     ProxyDocument=ProxyDocument[0];
     //console.log(ProxyDocument);
     //ProxyDocument2 =  await Proxys.findById("67055d06547913d9a5e095bb").exec();
     //await colllections.findOne({}).(hint: { timestamp: -1} )
     //db.foo.find().sort({_id:-1}).limit(x);
    
    console.log('got DOCUMENT');
    await closeMongo();
    
  } catch (error) {
    console.log(error);
    //await closeMongo();
  }
  return ProxyDocument;
} 