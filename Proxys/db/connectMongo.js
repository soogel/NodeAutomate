//https://blog.usman-s.me/how-to-use-mongoose-with-nextjs-for-mongodb
//https://www.youtube.com/watch?v=cM0pA50R20M
import mongoose from 'mongoose';

const connectMongo = async () => mongoose.connect(process.env.MONGO_URI);


const closeMongo = async () => mongoose.connection.close(true, (error, result) => { 
    if (error) { 
        console.log('error', error); 
    } else { 
        console.log('Connection Closed successfully'); 
    } 
} );

//export default connectMongo;

export { connectMongo, closeMongo };