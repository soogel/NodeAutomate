//https://blog.usman-s.me/how-to-use-mongoose-with-nextjs-for-mongodb
//import { Schema, model, models } from "mongoose";

import pkg from 'mongoose';
const { Schema, model, models } = pkg;

const proxysSchema = new Schema({
  proxy: {
    type: String,
    // required: true,
    required: [true, "Please provide proxy string."],
    unique: true,
  },
  protocol: {
    type: String,
  },
  ip: {
    type: String,
  },
  port: {
    type: Number,
  },
  speed: {
    type: Number,
  },
  country: {
    type: String,
  },
  active: {
    type: Boolean,
  },

  lastChecked: {
    type: Date,
    default: new Date(),
  },

  //////
  createdAt: {
    type: Date,
    default: new Date(),
  },
  updatedAt: {
    type: Date,
    default: new Date(),
  },
});

//userstatsSchema.index({ username: 1, likedpostID: 1}, { unique: true });

const Proxys = models.Proxys || model("Proxys", proxysSchema);

export default Proxys;
