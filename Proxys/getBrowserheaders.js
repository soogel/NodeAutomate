const axios = require("axios");
const fs = require("fs");

async function getBrowserheaders() {
  let url = "https://headers.scrapeops.io/v1/browser-headers";
  let keyy = "07efb7a2-8a5c-4f6c-a76b-8724621887df";
  let url_concat = url + "?api_key=" + keyy + "&num_headers=2";

  let res = await axios.get(url_concat);
  let data = res.data;

  //getData from data
  if (res.status == 200) {
    //console.log(data);
    return data.result;
  } else {
    console.log("Error loading proxy, please try again");
    return [];
  }
}

function readBrowserheaders() {
  let rawdata = fs.readFileSync("browser-headers.json");
  let browserheaders = JSON.parse(rawdata);

  return browserheaders;
}

function writeBrowserheader(browserheaders) {
  let data = JSON.stringify(browserheaders);
  fs.writeFileSync("browser-headers.json", data);
}
function uniq(a) {
  return Array.from(new Set(a));
}

async function generateBrowserheaders() {
  var browserheadersNew = [];
  var browserheaders = readBrowserheaders();
  for (i = 0; i < 10; i++) {
    browserheadersNew = browserheadersNew.concat(await getBrowserheaders());
  }

  browserheaders = browserheaders.concat(browserheadersNew);
  browserheaders = uniq(browserheaders);
  console.log(browserheaders.length);

  writeBrowserheader(browserheaders);
}

generateBrowserheaders();
