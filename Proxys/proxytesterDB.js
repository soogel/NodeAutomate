//const axios = require("axios");
import axios from "axios";
import fs from "fs";
//const fs = require("fs");
import getDBProxy  from './db/getProxyDB.js';
import { connectMongo, closeMongo } from './db/connectMongo.js'

//https://api.proxyscrape.com/v2/?request=displayproxies&protocol=http&timeout=10000&country=all&ssl=all&anonymity=all
//https://webhook.site
//document.getElementsByTagName('pre')[0].innerHTML.split(/\r?\n|\r|\n/g)

function proxyToObject(proxy) {
  //var proxy = proxysHttp[0];
  var proxyArray = proxy.split(":");
  var proxyObj = { proxyurl:"http"+ '://' + proxyArray[0] + ':' + proxyArray[1], protocol: "http", host: proxyArray[0], port: parseInt(proxyArray[1]) };
  //console.log(proxyObj);
  return proxyObj;
}

async function testproxy(proxyObj) {
  //'https://ident.me/ip';
  var data = "";
  try {
    var respond = await axios.get("https://api.ipify.org/?format=json", {
      proxy: {
        protocol: proxyObj.protocol,
        host: proxyObj.host,
        port: proxyObj.port,
      },
    });

    data = await respond.data;
    //console.log(data);
    //console.log(proxyObj);
  } catch (error) {
    if (error.response && error.response.status == 401) {
      console.log("Token not valid!");
    } else {
      //console.log(error.response);
      //console.log(error.response.status);
    }
  }
  //if ip ip than proxy working
  return data;
}
async function getOwnIP() {
  //'https://ident.me/ip';
  var data = "";
  try {
    var respond = await axios.get("https://api.ipify.org/?format=json", {
      
    });

    data = await respond.data;
    console.log('OwnIp #' + data);
    
  } catch (error) {
    if (error.response && error.response.status == 401) {
      console.log("Token not valid!");
    } else {
      //console.log(error.response);
      //console.log(error.response.status);
    }
  }
  //if ip ip than proxy working
  return data;
}

async function fetchproxy() {
  var data = "";
  try {
    var respond = await axios.get(
      "https://api.proxyscrape.com/v2/?request=displayproxies&protocol=http&timeout=10000&country=all&ssl=all&anonymity=all",
      {}
    );

    data = await respond.data;
    data = data.split(/\r?\n|\r|\n/g);
    //console.log(data);
  } catch (error) {
    if (error.response && error.response.status == 401) {
      console.log("Token not valid!");
    } else {
      console.log(error.response);
      console.log(error.response.status);
    }
  }
  //data = JSON.parse(data);
  return data;
}
function readProxys() {
  let rawdata = fs.readFileSync("proxys2.json");
  let proxys = JSON.parse(rawdata);
  // console.log(proxys);
  return proxys;
}
function readGoodProxys() {
  let rawdata = fs.readFileSync("proxysGood.json");
  let proxys = JSON.parse(rawdata);
  // console.log(proxys);
  return proxys;
}

function writeProxys(proxys) {
  let data = JSON.stringify(proxys);
  //let data = proxys;
  fs.writeFileSync("proxys2.json", data);
}
function writeGoodProxys(proxys) {
  let data = JSON.stringify(proxys);
  //let data = proxys;
  fs.writeFileSync("proxysGood.json", data);
}

async function writeGoodProxysAsync(proxys){
  let data = JSON.stringify(proxys);
  fs.writeFile("proxysGood2.json", data, { encoding: "utf8" }, (err) => {
    if (err) {
      console.log(err);
    }
    console.log("File saved");
  });

}
async function writeGoodProxysAsync2(proxys){
  let data = JSON.stringify(proxys);
  
    try {
      return await fs.writeFile("proxysGood2.json", data, "utf8") //options can use the shorthand version here, just a string automatically assigns file encoding
    } catch (err) {
      console.error('Error occurred while writing file:', err)
    }
  

}



async function proxyGetter() {
  var proxysOld = readProxys();
  var proxys = await fetchproxy();
  proxysOld = proxysOld.concat(proxys);
  console.log(proxys);
  writeProxys(proxysOld);
}

function getProxyListLength() {
  var proxy1 = readProxys();
  console.log(proxy1.length);
}
function getGoodProxyListLength() {
  var proxy1 = readGoodProxys();
  console.log(proxy1.length);
}

async function proxyTester() {
  var goodProxyList = [];
  var proxy = "";
  var proxyObj = {};
  var proxys = readProxys();

  var ownIpObj = await getOwnIP();

  for (var i = 0; i < proxys.length; i++) {
    proxy = proxys[i];
    proxy = proxys[getRandomInt(proxys.length)];
    
    proxyObj = proxyToObject(proxy);
    let respond =  await testproxy(proxyObj);
    console.log(respond);

    if ((respond.ip )) {
      console.log("true");
      //write to good proxys
      goodProxyList.push(proxyObj);
      await sendProxyEinzel(proxyObj);
      //await writeGoodProxysAsync2(goodProxyList); // only when bad internet
    }
    else {
      console.log("false");
    }
  }

  writeGoodProxys(goodProxyList)
}

async function sendProxyEinzel(Proxy) {
  var data='';
  var url = "http://localhost:3000/api/proxy/create";
  try{
   var response = await fetch(url, {
    method: "POST", // *GET, POST, PUT, DELETE, etc.
    mode: "cors", // no-cors, *cors, same-origin
    cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
    credentials: "same-origin", // include, *same-origin, omit
    headers: {
      "Content-Type": "application/json",
      // 'Content-Type': 'application/x-www-form-urlencoded',
    },
    redirect: "follow", // manual, *follow, error
    referrerPolicy: "no-referrer", // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
    body: JSON.stringify(Proxy), // body data type must match "Content-Type" header
  });
  //console.log(response);
  if(response.status === 200){
      data = response.json();
      console.log('Erfolg');
  }
 
}
catch (e){

}
  return data;
}

function getRandomInt(max) {
  return Math.floor(Math.random() * max);
}

//##########################################
//tester

//getGoodProxyListLength();
//proxyTester();
//proxyGetter();
getGoodProxyListLength();

var proxyObj = {
  proxyurl: 'http://185.217.199.176:4444',
  protocol: 'http',
  host: '185.217.199.176',
  port: 4444
}

//sendProxyEinzel(proxyObj);


testproxy(proxyToObject('91.189.177.186:3128'));
//testproxy(proxyToObject("8.219.97.248:80"));

function dbProxyToProxyObj(dbProxy){
  //console.log('proxyToObj');
  //console.log(dbProxy);
  var locDiv1 = dbProxy.indexOf('//');
  var protocol = dbProxy.slice(0,(locDiv1+2));
  //console.log('protocol');
  var substring1 = dbProxy.slice((locDiv1+2)); 
 // console.log(substring1);
  var locDiv2 = substring1.indexOf(':');
  var host = substring1.substring(0, locDiv2);
  //console.log(host);
  var port = substring1.slice(locDiv2+1); 
  //console.log(port);

 

  var proxyObj = {
    proxyurl: dbProxy,
    protocol: protocol,
    host: host,
    port: port
  }
return proxyObj;
}
/* var proxy1 = await getDBProxy();
console.log('proxy1');
console.log(proxy1);
console.log(proxy1.proxy);
console.log(dbProxyToProxyObj(proxy1.proxy)); */
//console.log(proxyToObject("8.219.97.248:80"));
//testproxy(proxy1);

//--> Proxy direkt richtig in DB speichern!!
